#!perl -w

use warnings;
use strict;
use Test::More tests => 13;

use Digest::MD5 qw(md5_hex);
use Time::HiRes qw(gettimeofday);

BEGIN {
    use_ok('Ticket::Simple');
}

my ( $s1, $ms1 ) = gettimeofday;
my $ts = Ticket::Simple->new();
my ( $t, $v ) = $ts->create_ticket( { login => 'test' } );
my ( $s2, $ms2 ) = gettimeofday;
ok( $t,              '- crate_ticket returns ticket' );
ok( $v,              '- create_ticket returns valid until time' );
ok( $v > "$s1.$ms1", '- now > before' );
ok( $v > "$s2.$ms2", '- now > after, if not ttl wrong' );

#diag( "ticket [$t]");
ok( ( length $t ) == 32, '- lenght ok' );

my ( $t1, $v1 )
    = $ts->create_ticket( { login => 'test', time => "$s1.$ms1" } );
ok( $t1,              '- crate_ticket returns ticket' );
ok( $v1,              '- create_ticket returns valid until time' );
ok( $v1 > "$s1.$ms1", '- now > before' );
ok( $v1 > "$s2.$ms2", '- now > after, if not ttl wrong' );
ok( ( length $t1 ) == 32, '- lenght ok' );
my ( $t2, $v2 )
    = $ts->create_ticket( { login => 'test', time => "$s1.$ms1" } );
ok( $t1 eq $t2, '- recreated ticket is the same' );
ok( $v1 == $v2, '- recreated vaild until time is the same' );

