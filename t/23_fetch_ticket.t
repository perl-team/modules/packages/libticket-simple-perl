#!perl -w

use warnings;
use strict;
use Test::More tests => 4;

BEGIN {
    use_ok('Ticket::Simple');
}

my $ts = Ticket::Simple->new();
my ( $t1, $v1 ) = $ts->create_ticket( { login => 't' } );
ok( $ts->store_ticket( { login => 't', ticket => $t1, valid => $v1 } ),
    '- can store ticket' );
my ( $t2, $v2 ) = $ts->fetch_ticket( { login => 't' } );
ok( $t1 eq $t2, '- ticket are equal' );
ok( $v1 == $v2, '- valid until time is the same' );

#diag("t1 [$t1] t2 [$t2] v1 [$v1] v2 [$v2]");

